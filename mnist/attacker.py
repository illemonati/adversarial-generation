import torch
import torch.nn as nn
import torch.nn.functional as F


class AttackerModel(nn.Module):
    def __init__(self):
        super().__init__()
        self.conv1 = nn.Conv2d(1, 16, 3)
        self.conv2 = nn.Conv2d(16, 32, 3)
        self.conv3 = nn.Conv2d(32, 64, 3)

        self.convt1 = nn.ConvTranspose2d(64, 32, 3)
        self.convt2 = nn.ConvTranspose2d(32, 16, 3)
        self.convt3 = nn.ConvTranspose2d(16, 1, 3)

    def forward(self, x):
        x = F.relu(self.conv1(x))
        x = F.relu(self.conv2(x))
        x = F.relu(self.conv3(x))
        x = F.relu(self.convt1(x))
        x = F.relu(self.convt2(x))
        x = self.convt3(x)
        x = torch.clamp(x, 0)
        return x
