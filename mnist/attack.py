import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import torchvision
from classifier import CNNClassifier
from attacker import AttackerModel

device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

batch_size = 1024

mnist_train_dataset, mnist_test_dataset = (
    torchvision.datasets.MNIST(
        './mnist-data', train=x, download=True, transform=torchvision.transforms.ToTensor()) for x in [True, False])

mnist_train_loader, mnist_test_loader = (
    torch.utils.data.DataLoader(dataset, batch_size=batch_size, shuffle=(dataset == mnist_train_dataset))
    for dataset in (mnist_train_dataset, mnist_test_dataset)
)

classifier = CNNClassifier().to(device)
classifier.load_state_dict(torch.load('classifier.pl'))

for param in classifier.parameters():
    param.requires_grad = False


class SameImageAttackLabelLoss(nn.Module):
    def __init__(self, attack_label_val):
        super().__init__()
        self.attack_label = torch.full((batch_size,), attack_label_val).to(device)

        self.image_loss_fn = nn.MSELoss()
        self.label_loss_fn = nn.CrossEntropyLoss()

    def forward(self, attack_images, original_images, attack_images_predicted_labels):
        image_loss = self.image_loss_fn(attack_images, original_images)
        attack_label = self.attack_label if attack_images_predicted_labels.shape[0] == batch_size else torch.full(
            (attack_images_predicted_labels.shape[0],), attack_label_val).to(device)
        label_loss = self.label_loss_fn(attack_images_predicted_labels, attack_label)
        return image_loss, label_loss,  0.95 * image_loss + 0.05 * label_loss


attack_label_val = 7

attacker = AttackerModel().to(device)
attacker.load_state_dict(torch.load('attacker.pl'))
optimizer = torch.optim.Adam(attacker.parameters(), lr=1e-3)
loss_fn = SameImageAttackLabelLoss(attack_label_val)

max_eps = 100

for ep in range(max_eps):
    for i, data in enumerate(mnist_train_loader):
        image_batch, label_batch = (x.to(device) for x in data)
        optimizer.zero_grad()
        result_images = attacker(image_batch)
        # this already has no grad
        y_attacker_images_pred = classifier(result_images)

        image_loss, label_loss, loss = loss_fn(result_images, image_batch, y_attacker_images_pred)
        loss.backward()
        optimizer.step()
        if i % 10 == 9:
            print(f'[{ep + 1}][{i + 1}] loss: {loss.item()} image: {image_loss.item()} label: {label_loss.item()}')

torch.save(attacker.state_dict(), 'attacker1.pl')
