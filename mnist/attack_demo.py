import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import torchvision
from classifier import CNNClassifier
from attacker import AttackerModel

batch_size = 2048

device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

mnist_train_dataset, mnist_test_dataset = (
    torchvision.datasets.MNIST(
        './mnist-data', train=x, download=True, transform=torchvision.transforms.ToTensor()) for x in [True, False])

mnist_train_loader, mnist_test_loader = (
    torch.utils.data.DataLoader(dataset, batch_size=batch_size, shuffle=(dataset == mnist_train_dataset))
    for dataset in (mnist_train_dataset, mnist_test_dataset)
)

classifier = CNNClassifier().to(device)
classifier.load_state_dict(torch.load('classifier.pl'))
attacker = AttackerModel().to(device)
attacker.load_state_dict(torch.load('attacker1.pl'))

sample_image_batch, sample_label_batch = next(iter(mnist_train_loader))
image_shape = sample_image_batch.shape[1:]
sample_label_shape = sample_label_batch.shape[1:]
print(image_shape, sample_label_shape)


figs, axs = plt.subplots(10, 2, figsize=(5, 20))

for i in range(10):
    sample_image = sample_image_batch[i]
    sample_label = sample_label_batch[i]

    with torch.no_grad():
        original_predicted_label = classifier(sample_image.unsqueeze(0).to(device)).argmax(1).item()
        attacker_generated_image = attacker(sample_image.unsqueeze(0).to(device))[0]
        attacker_predicted_label = classifier(attacker_generated_image.unsqueeze(0)).argmax(1).item()

    axs[i][0].imshow(sample_image.permute(1, 2, 0), cmap='gray')
    axs[i][0].set_title(f"true label: {sample_label}\npredicted label: {original_predicted_label}")

    axs[i][1].imshow(attacker_generated_image.cpu().permute(1, 2, 0), cmap='gray')
    axs[i][1].set_title(f"generated image\npredicted label: {attacker_predicted_label}")

plt.subplots_adjust(top=1, hspace=1)

plt.savefig('sample-attacker-image.png')
